        Welcome to DecentPOS. This project is currently in alpha. If you 
    would like to build it yourself, first install PostgreSQL and 
    libpq (found at "https://www.postgresql.org") and then install 
    libscrypt (found at "https://www.lolware.net/2014/04/29/libscrypt.com"). 
    Currently, only main.c and passEncrypt.c compile, though that 
    should change within the next few days assuming no major life 
    changes occur for me.

        I am currently a student working towards a degree in software 
    development. As a result, I'm sure the code is very messy and 
    un-optimized. If you come across this project, please take a look 
    and let me know any suggestions you might have. This project is as 
    much a learning experience for me as it as an actual "product" 
    whose development I hope to continue after my schooling. 

        In this project I do hope to develop a fully-functional 
    business-ready POS system for potential use in a real business 
    environment. However, as stated above, this project is in an alpha 
    stage, and is not suited for real-world usage at this time. As 
    such, I would discourage attempting to handle real-world 
    transactions until DecentPOS is feature-complete. If you choose to 
    do so despite my warning, the responsibility for ensuring any 
    monetary exchange is handeled correctly in full compliance with the 
    law is upon you, the user, and you only. I cannot and will not be 
    held responsible for error on the user's part until DecentPOS is in 
    what I consider to be a usable state, at which point I will do my 
    best to communicate clearly this fact. 

        Unless otherwise stated, all code is released under the GPLv3. 
    The only exception to this rule is for the third party libraries 
    used, which are used under their respective liscences. I will list 
    those libraries below. More information can be found in the LISCENSE 
    file and at "https://www.gnu.org/licenses/gpl.html". The GPLv3 
    grants    users the ability to modify  and distribute the program 
    as they so wish. I cant legally force you to do this, however, if 
    you do create a fork, I would appreciate at least being credited 
    for my work on the project ;). I would also reccomend a name change 
    and, if/when a logo for the project is created, changing those as 
    well so that users looking for one version don't accidently use 
    another. But whether you do this is up to you. I will not attempt 
    to enforce it as I have no legal avenue to do so, not to mention 
    attempting to would go against the principles of FOSS and the GPL.

    Third-party library listing and licenses:
    -libscrypt | https://lolware.net/2014/04/29/libscrypt.html | BSD 2-Clause "Simplified" License
    -libpq | https://postgresql.org | PostgreSQL License
