#include <stdio.h>
#include <ctype.h>
#include <string.h>

struct SaleInput{
    char saleCmd[32];
    int trucount;
};

void checkEntryType(struct SaleInput saleInput){
    int inputlength = 0;

    for(int counter = 0; counter < saleInput.trucount; counter++){
    while(saleInput.saleCmd[counter] != '\0'){
            if(isspace(saleInput.saleCmd[counter])){
                counter++;
            }
            else if(isalpha(saleInput.saleCmd[counter])){
                inputlength++;
                break;
            }
            else if(isdigit(saleInput.saleCmd[counter])){
                counter++;
                inputlength++;
            }
        }
    }
    saleInput.trucount = inputlength;
}   


/**
saleEntry takes input from the standard input (probably a keyboard of some kind, and eventually a barcode scanner) and checks what kind of input it is. If it is a product sku, it finds the price and any discount and prints it too the receipt.
 */
long int saleEntry(long int saleCount){
    FILE *receipt;
    receipt=fopen("receipt.txt","a+");
    fprintf(receipt, "Sale number %06ld\n", saleCount);
    fclose(receipt);

    int saleLine = 0;
    struct SaleInput saleInput;
    
    do{
        scanf(" %s", saleInput.saleCmd);
        saleInput.trucount = strlen(saleInput.saleCmd);

        checkEntryType(saleInput);

        printf("%d\n",saleInput.trucount);

        switch (saleInput.trucount){
            case 6:
                receipt=fopen("receipt.txt","a+");
                fprintf(receipt,"%d\t%s\n",saleLine,saleInput.saleCmd);
                fclose(receipt);
                saleLine++;
                break;
            default:
                break;
        }
    } while(saleInput.saleCmd[0] != 'x');
    
    receipt=fopen("receipt.txt","a+");
    fprintf(receipt, "End of sale\n");
    fclose(receipt);
    return saleCount;
}

