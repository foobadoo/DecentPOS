#include <stdio.h>
#include "saleEntry.h"

long int saleEntry(long int);

int promptScreen(){
    printf("Welcome to DecentPOS!");

    char openChar;
    do{
        long int saleCount;
        FILE *saleNum;
        saleNum=fopen("saleNum.txt","r");
        fscanf(saleNum,"%ld",&saleCount);
        fclose(saleNum);

        printf("Press \"n\" to start a new sale, \"h\" for help, or \"q\" to quit.\n");
        scanf(" %c", &openChar);

        switch(openChar){
            case'n':
                printf("Sale Number %ld\n",saleCount);
                saleEntry(saleCount);
                saleCount++;
                break;
            case'q':
                break;
            case'h':
                printf("Command list:\n\"n\"\tStart a new sale.\n\"q\"\tExit the program.\n\"h\"\tHelp with commands.\n");
                break;
            default:
                printf("Invalid entry. Enter \"h\" for a list of commands.");
                break;
        }
        
        /* if (openChar == 'n'){
            printf("Sale Number %ld\n",saleCount);
            saleEntry(saleCount);
            saleCount++;
        }
        else if (openChar != 'q' && openChar != 'n')
            printf("invalid entry\n");*/

        saleNum=fopen("saleNum.txt","w");
        fprintf(saleNum,"%ld",saleCount);
        fclose(saleNum);

    } while (openChar != 'q');

    printf("Bye!");
    return 0;
}
