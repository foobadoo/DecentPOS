#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <libscrypt.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include <libpq-fe.h>

#include "promptScreen.h"

extern int errno;
char loginUsername[16];
char *loginPassword;
char loginUserid[7];

const int maxUserLen = 16;
const int maxPassLen = 32;

const char conninfo;
PGconn *conn;
PGresult *res;
int nParams;
/*
   Funtion to exit in case of error.
*/
static void emergencyExit(PGconn *conn){
    PQfinish(conn);
    exit(1);
}

void loginPrompt(){
    
    printf("Welcome to DecentPOS!\n");
    printf("Enter username:\n");
    fgets(loginUsername,maxUserLen,stdin);

    printf("Enter password:\n");
    loginPassword = (char*) malloc(maxPassLen);
    getpass(loginPassword);

    const char command[] = "SELECT usr_name, usr_hash FROM usr WHERE (usr_name = $1);";
    nParams = 1;
    const int unameLen[] = {sizeof(loginUsername)}
    const char *const unameVal[] = {loginUsername}
    const int unameFormat[] = {0};
    int unameResFormat = 0;
    
    res = PQexecParams(conn, command, nParams, NULL, unameVal, unameLen, unameFormat, unameResFormat);
    if (PQresultStatus(res) != PGRES_COMMAND_OK){
        fprintf(stderr, "Incorrect login info. Make sure your username and password are correct and try again.\n");
        loginPrompt();
    }

    char *hashcheck = (char*) malloc(sizeof(PQgetvalue(res, 0, 1)));
    hashcheck = PQgetvalue(res, 0, 1);

    int passcheck = libscrypt_check(hashcheck, loginPassword);

    if (passcheck == 0){
        printf("Incorrect login info. Make sure your username and password are correct and try again.\n");
        loginPrompt();
    }
    
    else if (passcheck > 0){
        promptScreen();
    }

    else{
        int errnum = errno;
        fprintf(stderr, "Could not verify login info. Please contact your system administrator.\n Error number: %d\n Error message: %s\n", errno, strerror(errnum));
        emergencyExit(conn);
    }
}

int main(){
    conninfo = "dbname=store";
    conn = PQconnectdb(conninfo);
    
    /*check to see if connection was made, if not, exit*/
    if (PQstatus(conn) != CONNECTION_OK){
        fprintf(stderr,"Connection to database failed: %s", PQerrorMessage(conn));
        emergencyExit(conn);
    }

    if (access("startfile.txt",F_OK) != -1){
        loginPrompt();
    }
    else{
        FILE *startfile;
        startfile = fopen("startfile.txt","w+");
        fclose(startfile);

        printf("First time start up. Enter default account name (15 characters or less):\n");
        fgets(loginUsername,maxUserLen,stdin);
        char *gottenpass;
        
        printf("Enter default password:\n");
        gottenpass = (char*) malloc(maxPassLen);
        getpass(gottenpass);
        char *passhash;
        passhash = (char*) malloc(SCRYPT_MCF_LEN)
        libscrypt_hash(passhash, gottenpass, SCRYPT_N, SCRYPT_r, SCRYPT_p);
        loginUserid = "000000";

        /* 
           This is where we prepare the data to prepare our sql. Thanks
           to ictlyh on GitHubGist for his libpq-demo.cc that helped me 
           figure out how the hell to prepare the data.
        */
        const char command[] = "INSERT INTO usr VALUES ($1, $2, $3, $4, $5);"; 
        char auth[] = "0";
        char lnam[] = "user";
        nParams = 5;
        const char *const paramValues[] = {loginUserid, passhash, auth, loginUsername, lnam};
        const int paramLengths[] = {sizeof(loginUserid), sizeof(passhash), sizeof(auth), sizeof(loginUsername), sizeof(lnam)};
        const int paramFormats[] = {0, 0, 0, 0, 0};
        int resultFormat = 0;
        int success = 0;

        //And now the sql. Thanks again to ictlyh.
        res = PQexecParams(conn, command, nParams, NULL, paramValues, paramLengths, paramFormats, resultFormat);
        do{
            if (PQresultStatus(res) != PGRES_COMMAND_OK){
                fprintf(stderr, "Could not update user data. Do you have sufficient database privileges? Error message: %s", PQerrorMessage(conn));
            }
            else
                success = 1;
        }while (success != 1);

        loginPrompt();
        }
    PQfinish(conn);
    return 0;
}
